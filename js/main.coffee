$(document).ready(() ->
  footer('footer')
  content('section.wrapper')
)

footer = (box) ->
  as_little = 'a.point'
  as_big = 'a.point_big'
  addrs = '.addr'
  $("#{box} #{as_big}").click ->
    false
  $("#{box} #{as_little}").click ->
    to_show = $("#{box} #{addrs}#{$(this).attr('href')}")
    $("#{box} #{addrs}").removeClass('show')
    $(to_show).addClass('show')
    $("#{box} #{as_little}").removeClass('hide')
    $(this).addClass('hide')
    $("#{box} #{as_big}").removeClass('active')
    $("#{box} #{as_big}[href=\"#{$(this).attr('href')}\"]").addClass('active')
    false

content = (box) ->
  as_menu = 'aside a'
  contents = '.c'
  $("#{box} #{as_menu}").click ->
    # rm classes
    $("#{box} #{as_menu}").parents('li').removeClass('active').removeClass('bf').removeClass('af')
    $("#{box} #{contents}").removeClass('active')
    to_show = $(this).attr('href')
    # add classes
    $(this).parents('li').addClass('active').prev().addClass('bf')
    $(this).parents('li').next().addClass('af')
    $("#{box} #{contents}#{to_show}").addClass('active')
    false